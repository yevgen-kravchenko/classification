package edu.npl;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.nd4j.common.resources.Downloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

public enum DownloaderUtility {

    IRIS("https://archive.ics.uci.edu/static/public/53/iris.zip", "iris", "7a07b2b4163b650dc451aca467d4fb58"),
    WINE("https://archive.ics.uci.edu/static/public/109/wine.zip", "wine", "5efc729a8079d4ade3cadb59217226d4");

    private static final Logger log = LoggerFactory.getLogger(DownloaderUtility.class);
    private static final int DOWNLOAD_RETRIES = 5;
    private final String url;
    private final String folder;
    private final String md5;

    DownloaderUtility(String url, String dataFolder, String md5) {
        this.url = url;
        this.folder = dataFolder;
        this.md5 = md5;
    }

    public String Download() throws Exception {
        String zipFileName = Paths.get(this.url).getFileName().toString();
        String downloadPath = FilenameUtils.concat(System.getProperty("java.io.tmpdir"), zipFileName);
        String dataPathLocal = FilenameUtils.concat(System.getProperty("user.home"), "classification-data/" + folder);
        File dataPathLocalDir = new File(dataPathLocal);
        if (!dataPathLocalDir.exists()) {
            FileUtils.forceMkdir(dataPathLocalDir);
        }

        if (FileUtils.sizeOfDirectory(dataPathLocalDir) == 0) {
            log.info("_______________________________________________________________________");
            log.info("Downloading data and extracting to \n\t" + dataPathLocal);
            log.info("_______________________________________________________________________");
            Downloader.downloadAndExtract("files",
                    new URL(this.url),
                    new File(downloadPath),
                    dataPathLocalDir,
                    this.md5,
                    DOWNLOAD_RETRIES);
        } else {
            log.info("_______________________________________________________________________");
            log.info("Example data present in \n\t" + dataPathLocal);
            log.info("_______________________________________________________________________");
        }
        return dataPathLocal;
    }
}
